// console.log("askdfgbhaiu");

// USING OBJECT LITERALS

	let trainer = {
		name: "Ash Ketchum",
		age: 10,
		pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
		friends: {
			hoenn: ["May", "Max"],
			kanto: ["Brock", "Misty"],
			},
		talk: function(){
			console.log("Pikachu! I choose you!")		
		}
	}
	console.log(trainer);
	console.log("Result of dot notation:");
	console.log(trainer.name);
	console.log("Result of square bracket notation:");
	console.log(trainer["pokemon"]);
	console.log("Result of talk method");
	trainer.talk();


// USING CONSTRUCTOR FUNCTION

	function Pokemon(name, level){
		this.name = name;
		this.level = level;
		this.health = 2*level;
		this.attack = level;

		this.tackle = function(targetPokemon){
		console.log(this.name + " tackled " + targetPokemon.name);
		let hpAfterTackle = targetPokemon.health - this.attack;
		console.log(targetPokemon.name + "'s health is now reduced to " + hpAfterTackle)
		targetPokemon.health = hpAfterTackle

		if (hpAfterTackle <= 0){
			targetPokemon.faint()
		}
		}

		this.faint = function(){
			console.log(this.name + " fainted!");
		}
	}

	let pikachu = new Pokemon ("Pikachu", 12);
	console.log(pikachu);

	let geodude = new Pokemon ("Geodude", 8);
	console.log(geodude);

	let mewto = new Pokemon ("Mewto", 100);
	console.log(mewto);

// tackle method
	
	

	geodude.tackle(pikachu);
	console.log(pikachu);

	mewto.tackle(geodude);
	console.log(geodude);


